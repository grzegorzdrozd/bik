<?php
/**
 * Class FinancialReportBody
 */
class FinancialReportBody {

    /**
     * FinancialReportBody constructor.
     */
    public function __construct(Person $person, $authorizations) {

        $this->{'forma-pr-podm'} = new \stdClass();
        $this->{'forma-pr-podm'}->{'forma-pr-podm'} = '01';

        $this->{'dane-id-kons'} = new \stdClass();

        $this->{'dane-id-kons'}->{'form-przek-nazw-i-im'}   = '01';
        $this->{'dane-id-kons'}->{'imie'}                   = $person->getFirstName();
        $this->{'dane-id-kons'}->{'nazw'}                   = $person->getLastName();
        $this->{'dane-id-kons'}->{'pesel'}                  = $person->getPersonId()->getId();
        $this->{'dane-id-kons'}->{'rodz-dok-tozs'}          = $person->getIdentification()->getType();
        $this->{'dane-id-kons'}->{'seria-nr-dok-tozs'}      = $person->getIdentification()->getId();
        $this->{'dane-id-kons'}->{'obywatelstwo'}           = $person->getNationality();

        $this->{'daty-upow'} = new \stdClass();
        foreach($authorizations as $authorization) {
            foreach($authorization->getFields() as $fieldName => $value) {
                $this->{'daty-upow'}->$fieldName = $value;
            }
        }
    }
}
