<?php

abstract class AbstractIdentification {

    /**
     * @var
     */
    protected $id;

    /**
     * @param $string
     *
     * @return mixed
     */
    public abstract function isValid($string);


    /**
     * Pesel constructor.
     *
     * @param string $string
     */
    public function __construct($string) {
        $string = $this->filter($string);
        if ($this->isValid($string)) {
            $this->id = $this->format($string);
        } else {
            throw new \InvalidArgumentException('Invalid number');
        }
    }

    /**
     * @param $string
     *
     * @return mixed
     */
    public function format($string){
        return $string;
    }

    /**
     * @param $string
     *
     * @return string
     */
    public function filter($string){
        return strtolower(trim($string));
    }

    /**
     *
     *
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     *
     * @param mixed $id
     *
     * @return AbstractIdentification
     */
    public function setId($id) {
        $this->id = $id;
    }

    
}
