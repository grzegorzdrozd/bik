<?php

class FinancialReportRequest extends AbstractReportRequest {

    /**
     * @var FinancialReportHeader
     */
    public $naglowek;

    /**
     * @var FinancialReportBody
     */
    public $dane;

    /**
     * FinancialReportRequest constructor.
     */
    public function __construct(Person $person, array $authorizations) {
        $this->naglowek = new FinancialReportHeader();

        $this->dane = new FinancialReportBody($person, $authorizations);
    }
}
