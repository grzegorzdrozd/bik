<?php

class Person {

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var AbstractIdentification
     */
    public $personId;

    /**
     * @var AbstractIdentificationDocument
     */
    public $identification;

    /**
     * @var string
     */
    public $nationality;


    /**
     * Person constructor.
     *
     * @param string $firstName
     * @param string $lastName
     * @param AbstractIdentification $personId
     * @param AbstractIdentificationDocument $identification
     * @param string $nationality
     */
    public function __construct($firstName, $lastName,  AbstractIdentification $personId, AbstractIdentificationDocument $identification, $nationality) {
        $this->firstName      = $firstName;
        $this->lastName       = $lastName;
        $this->personId       = $personId;
        $this->identification = $identification;
        $this->nationality    = $nationality;
    }

    /**
     *
     *
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     *
     *
     * @param string $firstName
     *
     * @return Person
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    /**
     *
     *
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     *
     *
     * @param string $lastName
     *
     * @return Person
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    /**
     *
     *
     * @return AbstractIdentification
     */
    public function getPersonId() {
        return $this->personId;
    }

    /**
     *
     *
     * @param AbstractIdentification $personId
     *
     * @return Person
     */
    public function setPersonId($personId) {
        $this->personId = $personId;
    }

    /**
     *
     *
     * @return AbstractIdentificationDocument
     */
    public function getIdentification() {
        return $this->identification;
    }

    /**
     *
     *
     * @param AbstractIdentificationDocument $identification
     *
     * @return Person
     */
    public function setIdentification($identification) {
        $this->identification = $identification;
    }

    /**
     *
     *
     * @return string
     */
    public function getNationality() {
        return $this->nationality;
    }

    /**
     *
     *
     * @param string $nationality
     *
     * @return Person
     */
    public function setNationality($nationality) {
        $this->nationality = $nationality;
    }

    
}

