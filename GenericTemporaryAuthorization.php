<?php

class GenericTemporaryAuthorization {

    /**
     * GenericTemporaryAuthorization constructor.
     */
    public function __construct() {
        $this->{'data-up-24-1'}         = '2017-04-06T00:00:00';
        $this->{'data-up-bik'}          = '2017-04-06T00:00:00';
        $this->{'data-up-zbp'}          = '2017-04-06T00:00:00';
        $this->{'data-up-rap-o-zap'}    = '2017-04-06';
    }

    public function getFields(){
        return get_object_vars($this);
    }
}
