<?php

class Client extends \SoapClient {

    /**
     * @var string
     */
    protected $userName = 'USER183275';

    /**
     * @var string
     */
    protected $password = '4m4iq4d8L8';

    /**
     * @var bool
     */
    protected $debug = true;

    /**
     * Client constructor.
     */
    public function __construct() {
        // @todo move wsdl to parameter or config file
        $wsdl = "https://wasstt.infomonitor.pl:443/39D97477-0709-455f-A7C8-6498B600AC5A/ws/BIG?WSDL";

        // @todo add flag for debug
        $options = [
            'classmap'      => [
                // key is WSDL type, value is name of php class
                'zapytanie-fin'     => FinancialReportRequest::class,
                'raport-fin'        => FinancialReportResponse::class
            ],
            'exceptions'    => true,
            'trace'         => true,
            'cache'         => WSDL_CACHE_DISK,
            'features'      => SOAP_SINGLE_ELEMENT_ARRAYS | SOAP_USE_XSI_ARRAY_TYPE
        ];

        parent::__construct($wsdl, $options);
    }


    public function getFinancialReport($request) {
        return $this->__soapCall('pobranie-raportu-fin', [$request]);
    }
}
