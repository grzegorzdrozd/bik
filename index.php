<?php

try {
    $client = new \Client();
} catch(\Exception $e) {
    var_dump($e);
}

try {

    $person = new \Person('JULIA', 'BRYSTYGIER', new Pesel('96010809665'), new PolishId('UWA807018'), 'PL');

    // temporary array of objects that hold id to authorizations to make a request.
    // we need to create object for each authorization type ?
    // if this is static we can remove it and just hard code it
    $authorizations = [];
    $authorizations[] = new GenericTemporaryAuthorization();

    // do a request
    $financialReportRequest = new \FinancialReportRequest($person, $authorizations);


    // this response will be of type FinancialReportResponse
    /** @var FinancialReportResponse $response */
    $response = $client->getFinancialReport($financialReportRequest);

    // handle response
    printf("Liczba zobowiązań: %s\n", $response->getNumberOfOverdueCommitment());
    printf("Suma zobowiązań: %s\n", $response->getSumOfOverdueCommitment());

} catch (\SoapFault $e) {
    d($client);
    var_dump($e);
} catch (\Exception $e) {
    d($client);

    var_dump($e);
}

function d(SoapClient $c) {
    $dom = new \DOMDocument();
    $dom->formatOutput = true;
    $dom->loadXML($c->__getLastRequest());
    var_dump($dom->saveXML());
}


function __autoload($className) {
    if (file_exists($className.'.php')) {
        require_once $className.'.php';
    } else {
        die('missing class: '.$className);
    }
}
