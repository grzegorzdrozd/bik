<?php

class AbstractReportHeader {
    /**
     * @var
     */
    public $dane_id;

    /**
     * @var
     */
    public $typ_zapyt;

    /**
     * @var int
     */
    protected $requestId = 0;

    /**
     * FinancialReportHeader constructor.
     */
    public function __construct() {
        $this->{'dane-id'} = new \stdClass();
        $this->{'dane-id'}->{'nr-ref'}              = '20170406-1';
        $this->{'dane-id'}->{'id-kli-im'}           = '1969114612';
        $this->{'dane-id'}->{'id-jed-org-kli'}      = '1969114612';
        $this->{'dane-id'}->{'id-operatora'}        = 'USER183275';
        $this->{'dane-id'}->{'haslo-operatora'}     = '4m4iq4d8L8';
        $this->{'dane-id'}->{'zn-danych-test'}      = true; // it is important to pass correct types!
        $this->{'dane-id'}->{'zn-powt-operacji'}    = false; // it is important to pass correct types!
        
        $this->{'typ-zapyt'}                     = new \stdClass();
        $this->{'typ-zapyt'}->{'kod-rodz-zapyt'} = $this->requestId;
        $this->{'typ-zapyt'}->{'czas-zapyt'}     = date('Y-m-d\TH:i:s');
    }
}
